package model;

public interface Into<T> {
    T into();
}
