package model;

public interface From<T> {
    void from(T data);
}
