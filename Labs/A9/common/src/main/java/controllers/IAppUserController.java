package controllers;

import dto.AppUserDTO;

public interface IAppUserController {
    AppUserDTO getAppUser(Long id);
}
