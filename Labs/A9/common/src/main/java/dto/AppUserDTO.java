package dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data()
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppUserDTO extends BaseDTO {
    private String name;
    private String birthday;
}
