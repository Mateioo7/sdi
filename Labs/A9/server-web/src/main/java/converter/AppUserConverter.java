package converter;

import dto.AppUserDTO;
import model.AppUser;
import org.springframework.stereotype.Component;

@Component
public class AppUserConverter extends BaseConverter<Long, AppUser, AppUserDTO> {
    @Override
    public AppUser convertDTOToModel(AppUserDTO dto) {
        AppUser model = new AppUser();
        model.setId(dto.getId());
        model.setName(dto.getName());
        model.setBirthday(dto.getBirthday());

        return model;
    }

    @Override
    public AppUserDTO convertModelToDTO(AppUser model) {
        AppUserDTO dto = new AppUserDTO();
        dto.setId(model.getId());
        dto.setName(model.getName());
        dto.setBirthday(model.getBirthday());

        return dto;
    }
}
