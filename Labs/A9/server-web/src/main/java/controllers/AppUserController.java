package controllers;

import converter.AppUserConverter;
import dto.AppUserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import services.IAppUserService;

public class AppUserController implements IAppUserController {
    public static final Logger log = LoggerFactory.getLogger(IClientController.class);

    @Autowired
    private IAppUserService service;

    @Autowired
    private AppUserConverter converter;

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public AppUserDTO getAppUser(@PathVariable Long id) {
        AppUserDTO cpy = this.converter.convertModelToDTO(this.service.find(id));
        return cpy;
    }
}
