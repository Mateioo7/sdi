import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ClientsDashboardComponent} from './components/clients/dashboard/clients-dashboard.component';
import {HttpClientModule} from "@angular/common/http";
import {UpdateClientComponent} from './components/clients/update/update-client.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CreateClientComponent} from './components/clients/create/create-client.component';
import {DomainsDashboardComponent} from './components/domains/dashboard/domains-dashboard.component';
import {CreateDomainComponent} from './components/domains/create/create-domain.component';
import {UpdateDomainComponent} from './components/domains/update/update-domain.component';
import {HomeComponent} from './components/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientsDashboardComponent,
    UpdateClientComponent,
    CreateClientComponent,
    DomainsDashboardComponent,
    CreateDomainComponent,
    UpdateDomainComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
