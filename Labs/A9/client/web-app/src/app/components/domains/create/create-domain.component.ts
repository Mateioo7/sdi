import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AbstractControl, FormBuilder, Validators} from "@angular/forms";
import {DomainService} from "../../../clients/domain.service";
import {IDomain} from "../../../model/IDomain";

@Component({
  selector: 'app-create-domain',
  templateUrl: './create-domain.component.html',
  styleUrls: ['./create-domain.component.css']
})
export class CreateDomainComponent implements OnInit {
  constructor(private domainService: DomainService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder) {}

  domain: IDomain = {} as any;
  domains: IDomain[] = [];
  maxId = 1;

  domainForm = this.formBuilder.group({
    name: ['', [Validators.required, Validators.minLength(4)]],
    price: ['', [Validators.required, Validators.min(3), Validators.max(100)]]
  });

  ngOnInit(): void {
    this.computeValidId();
  }

  createDomain(): void {
    this.domain.id = this.maxId;
    this.domain.name = this.domainForm.get("name")!.value;
    this.domain.price = this.domainForm.get("price")!.value;
    this.domainService.createDomain(this.domain)
      .subscribe(() => {
        alert("Domain created");
        this.router.navigate(['/domains/dashboard']).then();
      });
  }

  private computeValidId(): void {
    let allDomains: IDomain[] = [];
    this.domainService.getAllDomains()
      .subscribe(clients => {
        allDomains = clients;
        Array.prototype.forEach.call(allDomains, domain => {
          if (domain.id > this.maxId) {
            this.maxId = domain.id;
          }
        });
        this.maxId += 1;
      });
  }

  get name(): AbstractControl { return this.domainForm.get('name') as AbstractControl; }

  get price(): AbstractControl { return this.domainForm.get('price') as AbstractControl; }
}
