import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AbstractControl, FormBuilder, Validators} from "@angular/forms";
import {DomainService} from "../../../clients/domain.service";
import {IDomain} from "../../../model/IDomain";

@Component({
  selector: 'app-update-domain',
  templateUrl: './update-domain.component.html',
  styleUrls: ['./update-domain.component.css']
})
export class UpdateDomainComponent implements OnInit {
  constructor(private domainService: DomainService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  domainForm = this.formBuilder.group({
    name: ['', [Validators.required, Validators.minLength(4)]],
    price: ['', [Validators.required, Validators.min(3), Validators.max(100)]]
  });

  ngOnInit(): void {
    const id = this.getIdFromUrl();
    this.getDomainWithId(id);
  }

  getDomainWithId(id: string): void {
    this.domainService.getDomainWithId(id)
      .subscribe(domain => {
        this.domainForm.get("name")!.setValue(domain.name);
        this.domainForm.get("price")!.setValue(domain.price);
      });
  }

  updateDomain(): void {
    const domain: IDomain = {} as any;
    domain.id = parseInt(this.getIdFromUrl(), 10);
    domain.name = this.domainForm.get("name")!.value;
    domain.price = this.domainForm.get("price")!.value;

    this.domainService.updateDomain(domain)
      .subscribe(() => {
        alert("Domain updated");
        this.router.navigate(['/domains/dashboard']).then();
      });
  }

  getIdFromUrl(): string {
    const routeParams = this.route.snapshot.paramMap;
    return routeParams.get('domainId') as string;
  }

  get name(): AbstractControl { return this.domainForm.get('name') as AbstractControl; }

  get price(): AbstractControl { return this.domainForm.get('price') as AbstractControl; }
}
