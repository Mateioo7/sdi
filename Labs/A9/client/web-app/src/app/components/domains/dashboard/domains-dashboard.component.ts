import {Component, OnInit} from '@angular/core';
import {IDomain} from "../../../model/IDomain";
import {DomainService} from "../../../clients/domain.service";

@Component({
  selector: 'app-webdomains-dashboard',
  templateUrl: './domains-dashboard.component.html',
  styleUrls: ['./domains-dashboard.component.css']
})
export class DomainsDashboardComponent implements OnInit {
  constructor(private domainService: DomainService) {}

  domains: IDomain[] = [];
  domain: IDomain = {} as any;
  name: any;

  ngOnInit(): void {
    this.getAllDomains();
  }

  getAllDomains(): void {
    this.domainService.getAllDomains()
      .subscribe(domains =>  {
        this.domains = domains;
      });
  }

  onInput(): void {
    console.log('name: ' + this.name);
    if (this.name === "") {
      this.getAllDomains();
    }
    else {
      this.domainService.filterDomainsByName(this.name)
        .subscribe(clients => {
          this.domains = clients;
        });
    }
  }

  sortByPrice(): void {
    this.domains
      .sort((first, second) => first.price - second.price);
  }

  removeDomain(id: number): void {
    this.domainService.removeDomain(String(id))
      .subscribe(() => {
        alert('Chosen domain removed');
        location.reload();
      });
  }
}
