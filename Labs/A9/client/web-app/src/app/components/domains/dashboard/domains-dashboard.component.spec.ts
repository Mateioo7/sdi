import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DomainsDashboardComponent} from './domains-dashboard.component';

describe('WebdomainsDashboardComponent', () => {
  let component: DomainsDashboardComponent;
  let fixture: ComponentFixture<DomainsDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DomainsDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
