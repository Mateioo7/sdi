import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../../clients/client.service';
import {IClient} from '../../../model/IClient';

@Component({
  selector: 'app-dashboard',
  templateUrl: './clients-dashboard.component.html',
  styleUrls: ['./clients-dashboard.component.css']
})
export class ClientsDashboardComponent implements OnInit {
  constructor(private clientService: ClientService) {}

  clients: IClient[] = [];
  client: IClient = {} as any;
  name: any;

  ngOnInit(): void {
    this.getAllClients();
  }

  getAllClients(): void {
    this.clientService.getAllClients()
      .subscribe(clients =>  {
        this.clients = clients;
      });
  }

  removeClient(id: number): void {
    this.clientService.removeClient(String(id))
      .subscribe(() => {
        alert('Chosen client removed');
        location.reload();
      });
  }

  sortByName(): void {
    this.clients
          .sort((first, second) => first.name.localeCompare(second.name));
  }

  onInput(): void {
    console.log('name: ' + this.name);
    if (this.name === "") {
      this.getAllClients();
    }
    else {
      this.clientService.filterClientsByName(this.name)
        .subscribe(clients => {
          this.clients = clients;
        });
    }
  }
}
