import {Component, OnInit} from '@angular/core';
import {ClientService} from "../../../clients/client.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, NgForm} from "@angular/forms";
import {IClient} from "../../../model/IClient";

@Component({
  selector: 'app-update-client',
  templateUrl: './update-client.component.html',
  styleUrls: ['./update-client.component.css']
})
export class UpdateClientComponent implements OnInit {
  constructor(private clientService: ClientService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder) {}

  client: IClient = {} as any;

  ngOnInit(): void {
    const id = this.getIdFromUrl();
    this.getClientWithId(id);
  }

  getClientWithId(id: string): void {
    this.clientService.getClientWithId(id)
      .subscribe(client => this.client = client);
  }

  updateClient(form: NgForm): void {
    this.client.id = parseInt(this.getIdFromUrl(), 10);
    this.client.name = form.value.name;
    this.client.isBusiness = form.value.isBusiness;

    this.clientService.updateClient(this.client)
      .subscribe(() => {
        alert("Client updated");
        this.router.navigate(['/clients/dashboard']).then();
      });
  }

  getIdFromUrl(): string {
    const routeParams = this.route.snapshot.paramMap;
    return routeParams.get('clientId') as string;
  }
}
