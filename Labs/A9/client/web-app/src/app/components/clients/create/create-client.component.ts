import {Component, OnInit} from '@angular/core';
import {FormBuilder, NgForm} from "@angular/forms";
import {ClientService} from "../../../clients/client.service";
import {ActivatedRoute, Router} from "@angular/router";
import {IClient} from "../../../model/IClient";

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.component.html',
  styleUrls: ['./create-client.component.css']
})
export class CreateClientComponent implements OnInit {
  constructor(private clientService: ClientService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder) {}

  client: IClient = {} as any;
  clients: IClient[] = [];
  maxId = 1;

  ngOnInit(): void {
    this.computeValidId();
    this.client.isBusiness = false;
  }

  createClient(form: NgForm): void {
    this.client.id = this.maxId;
    this.client.name = form.value.name;
    if (form.value.isBusiness !== '') {
      this.client.isBusiness = form.value.isBusiness;
    }
    this.clientService.createClient(this.client)
      .subscribe(() => {
        alert("Client created");
        this.router.navigate(['/clients/dashboard']).then();
      });
  }

  private computeValidId(): void {
    let allClients: IClient[] = [];
    this.clientService.getAllClients()
      .subscribe(clients => {
        allClients = clients;
        Array.prototype.forEach.call(allClients, client => {
          if (client.id > this.maxId) {
            this.maxId = client.id;
          }
        });
        this.maxId += 1;
      });
  }
}
