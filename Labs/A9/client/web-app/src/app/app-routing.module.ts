import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientsDashboardComponent} from './components/clients/dashboard/clients-dashboard.component';
import {UpdateClientComponent} from './components/clients/update/update-client.component';
import {CreateClientComponent} from "./components/clients/create/create-client.component";
import {DomainsDashboardComponent} from "./components/domains/dashboard/domains-dashboard.component";
import {CreateDomainComponent} from "./components/domains/create/create-domain.component";
import {UpdateDomainComponent} from "./components/domains/update/update-domain.component";
import {HomeComponent} from "./components/home/home.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'clients/dashboard', component: ClientsDashboardComponent },
  { path: 'clients/update/:clientId', component: UpdateClientComponent },
  { path: 'clients/create', component: CreateClientComponent },
  { path: 'domains/dashboard', component: DomainsDashboardComponent },
  { path: 'domains/update/:domainId', component: UpdateDomainComponent },
  { path: 'domains/create', component: CreateDomainComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
