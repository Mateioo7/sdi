export interface IDomain {
  id: number;
  name: string;
  price: number;
}

export interface IDomains {
  webdomains: IDomain[];
}
