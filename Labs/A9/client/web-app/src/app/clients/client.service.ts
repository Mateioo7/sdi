import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IClient, IClients} from '../model/IClient';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private baseUrl = 'http://localhost:8080/server-web/api/clients';
  private sortClientsAfterNameUrl = this.baseUrl + '/sort/name';
  private filterClientsByNameUrl = this.baseUrl + '/filter/';

  constructor(private httpClient: HttpClient) {}

  getAllClients(): Observable<IClient[]> {
    return this.httpClient.get<IClients>(this.baseUrl)
      .pipe(map(receivedData => receivedData.clients));
  }

  getClientWithId(id: string): Observable<IClient> {
    return this.httpClient.get<IClient>(this.baseUrl + "/" + id);
  }

  updateClient(client: IClient): any {
    return this.httpClient.put(this.baseUrl, client);
  }

  removeClient(id: string): any {
    return this.httpClient.delete(this.baseUrl + "/" + id);
  }

  createClient(client: IClient): any {
    return this.httpClient.post(this.baseUrl, client);
  }

  // json order is not preserved <=> on backend works - on frontend it doesn't
  getSortedClientsByName(): Observable<IClient[]> {
    return this.httpClient.get<IClients>(this.sortClientsAfterNameUrl)
      .pipe(map(receivedData => receivedData.clients));
  }

  filterClientsByName(name: string): Observable<IClient[]> {
    return this.httpClient.get<IClients>(this.filterClientsByNameUrl + name)
      .pipe(map(receivedData => receivedData.clients));
  }

}
