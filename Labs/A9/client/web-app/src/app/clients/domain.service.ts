import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {IDomain, IDomains} from "../model/IDomain";

@Injectable({
  providedIn: 'root'
})
export class DomainService {
  private baseUrl = 'http://localhost:8080/server-web/api/domains';
  private filterDomainsByNameUrl = this.baseUrl + '/filter/';

  constructor(private httpClient: HttpClient) {}

  getAllDomains(): Observable<IDomain[]> {
    return this.httpClient.get<IDomains>(this.baseUrl)
      .pipe(map(receivedData => receivedData.webdomains));
  }

  getDomainWithId(id: string): Observable<IDomain> {
    return this.httpClient.get<IDomain>(this.baseUrl + "/" + id);
  }

  updateDomain(domain: IDomain): any {
    return this.httpClient.put(this.baseUrl, domain);
  }

  removeDomain(id: string): any {
    return this.httpClient.delete(this.baseUrl + "/" + id);
  }

  createDomain(domain: IDomain): any {
    return this.httpClient.post(this.baseUrl, domain);
  }

  filterDomainsByName(name: string): Observable<IDomain[]> {
    return this.httpClient.get<IDomains>(this.filterDomainsByNameUrl + name)
      .pipe(map(receivedData => receivedData.webdomains));
  }
}
