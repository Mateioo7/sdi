# [15 p] Lab 10: Angular, Spring, JPA
- continue to work individually on the previous project (same repo)
- Spring Boot is, from now on, allowed (the project structure should be the same as before i.e. core, web)
- CRUD operations (the link entity/entities e.g. Rental, StudentDiscipline is/are for now not required; relations - for now, not required)
- Filter, sort operations (client-side and server-side; on server-side, with Spring Data JPA)
- Use both reactive and template-driven forms and validate user input (self study)
- Use ES6 features (or above) and follow redux principles (see readme)
- There should be four root entities, and the requirements may be split over them, e.g., CRUD operations on entity1, client-side filters on entity2, server-side filters on entity3 etc

# [15 p] Lab 9: Angular, Spring, JPA
- convert the previous project to a web application using Angular
- use Angular version 2 or higher (AngularJS/Angular1 is forbidden)
- use Spring --- xml config forbidden
- use Spring Data JPA (Hibernate) --- xml config forbidden
- log messages using SLF4J
- only one feature is enough, e.g., show list of students
- Spring Boot is (for now) forbidden
