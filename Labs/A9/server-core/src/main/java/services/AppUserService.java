package services;

import exceptions.BlogException;
import model.AppUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import repository.database.AppUserRepository;

public class AppUserService implements IAppUserService {
    public static final Logger log = LoggerFactory.getLogger(AppUserService.class);

    @Autowired
    private AppUserRepository appUserRepository;

    @Override
    public AppUser find(Long id) {
        log.trace("AppUser find - method entered: id={}", id);

        AppUser result = this.appUserRepository.findById(id)
                .orElseThrow(() -> new BlogException(String.format("AppUser id not found: %d", id)));

        log.trace("AppUser find - method finished: result={}", result);

        return result;
    }

    @Override
    public AppUser findWithFollowers(Long id) {
        return null;
    }

    @Override
    public AppUser findWithPostsAndFollowers(Long id) {
        return null;
    }
}
