package services;

import model.AppUser;

public interface IAppUserService {
    AppUser find(Long id);
    AppUser findWithFollowers(Long id);
    AppUser findWithPostsAndFollowers(Long id);
}
