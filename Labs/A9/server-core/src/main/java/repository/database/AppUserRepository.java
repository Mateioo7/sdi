package repository.database;

import model.AppUser;

public interface AppUserRepository extends DatabaseRepository<AppUser, Long> {
    AppUser findWithFollowers(Long id);
    AppUser findWithPostsAndFollowers(Long id);
}
