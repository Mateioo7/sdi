package model;


import org.w3c.dom.Node;

public interface FromNode {
    void fromNode(Node data);
}
