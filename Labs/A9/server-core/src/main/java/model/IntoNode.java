package model;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public interface IntoNode {
    Node intoNode(Document document);
}
