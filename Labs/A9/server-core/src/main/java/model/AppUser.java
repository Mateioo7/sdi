package model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "AppUser")
public class AppUser extends BaseEntity<Long> {
    public AppUser() {
        this.name = "";
        this.birthday = "";
    }

    public AppUser(String name, String birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    private String name;
    private String birthday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
