package controllers;

import dto.ClientDTO;
import dto.ClientsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ClientController implements IClientController {
    @Autowired
    private RestTemplate restTemplate;
    private final String url = "http://localhost:8080/api/clients";

    @Override
    public ClientsDTO getClients() {
        return this.restTemplate.getForObject(this.url, ClientsDTO.class);
    }

    @Override
    public ClientDTO getClient(Long id) {
        return this.restTemplate.getForObject(this.url + "/{id}", ClientDTO.class, id);
    }

    @Override
    public ResponseEntity<?> addClient(ClientDTO Client){
        try {
            this.restTemplate.put(this.url, Client);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> deleteClient(Long domainId) {
        try {
            this.restTemplate.delete(this.url, domainId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> updateClient(ClientDTO Client) {
        try {
            this.restTemplate.put(this.url, Client);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ClientsDTO filterClientsByName(String name) {
        return this.restTemplate.getForObject(this.url + "/filter/" + name, ClientsDTO.class);
    }

    @Override
    public ClientsDTO sortAscendingByClientName() {
        return this.restTemplate.getForObject(this.url + "/sort/name", ClientsDTO.class);
    }
}
