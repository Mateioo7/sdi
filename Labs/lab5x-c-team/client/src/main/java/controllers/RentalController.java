package controllers;

import dto.RentalDTO;
import dto.RentalsDTO;
import dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Set;

public class RentalController implements IRentalController {
    @Autowired
    private RestTemplate restTemplate;
    private String url = "http://localhost:8080/api/rentals";

    @Override
    public RentalsDTO getRentals() {
        return restTemplate.getForObject(url, RentalsDTO.class);
    }

    @Override
    public RentalDTO getRental(Long id) {
        return restTemplate.getForObject(url, RentalDTO.class);
    }

    @Override
    public ResponseEntity<?> addRental(RentalDTO rental){
        try {
            restTemplate.put(url, rental);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> deleteRental(Long rentalId) {
        try {
            restTemplate.delete(url, rentalId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> updateRental(RentalDTO rental) {
        try {
            restTemplate.put(url, rental);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public RentalsDTO findRentalsByClientName(String name) {
        return this.restTemplate.getForObject(this.url + "/filter/client/" + name, RentalsDTO.class);
    }

    @Override
    public RentalsDTO findRentalsByDomainName(String name) {
        return this.restTemplate.getForObject(this.url + "/filter/domain/" + name, RentalsDTO.class);
    }

    @Override
    public RentalsDTO filterRentalsByClientId(Long id) {
        return null;
    }

    @Override
    public RentalsDTO filterRentalsByDomainId(Long id) {
        return null;
    }

    @Override
    public ClientsDTO getMostRentingClients () {
        return null;
    }

    @Override
    public WebDomainsDTO getMostRentedDomains () {
        return null;
    }

    @Override
    public Set<String> getMostRentedYears () {
        return null;
    }

    @Override
    public Set<String> getMostCommonTld () {
        return null;
    }
}
