package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.remoting.rmi.RmiServiceExporter;
import services.IClientService;
import services.IRentalService;
import services.IWebDomainService;

@Configuration
@Import({RepositoriesConfiguration.class})
@ComponentScan({"services", "repository.database"})
public class ServerConfig {
    @Autowired
    private IRentalService rentalService;
    @Autowired
    private IClientService clientService;
    @Autowired
    private IWebDomainService webDomainService;

    @Bean
    RmiServiceExporter registerClientService() {
        RmiServiceExporter rmiServiceExporter = new RmiServiceExporter();
        rmiServiceExporter.setServiceInterface(IClientService.class);
        rmiServiceExporter.setService(this.clientService);
        rmiServiceExporter.setServiceName("ClientService");
        return rmiServiceExporter;
    }

    @Bean
    RmiServiceExporter registerWebDomainService() {
        RmiServiceExporter rmiServiceExporter = new RmiServiceExporter();
        rmiServiceExporter.setServiceInterface(IWebDomainService.class);
        rmiServiceExporter.setService(this.webDomainService);
        rmiServiceExporter.setServiceName("WebDomainService");
        return rmiServiceExporter;
    }

    @Bean
    RmiServiceExporter registerRentalService() {
        RmiServiceExporter rmiServiceExporter = new RmiServiceExporter();
        rmiServiceExporter.setServiceInterface(IRentalService.class);
        rmiServiceExporter.setService(this.rentalService);
        rmiServiceExporter.setServiceName("RentalService");
        return rmiServiceExporter;
    }
}